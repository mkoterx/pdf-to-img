var amqp = require('amqplib/callback_api');

const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const QUEUE_NAME = 'hello';
const QUEUE_OPTS = {
  durable: false
}

amqp.connect('amqp://localhost', (connectionError, connection) => {
  if (connectionError) throw connectionError;

  connection.createChannel((createChannelError, channel) => {
    if (createChannelError) throw createChannelError;

    channel.assertQueue(QUEUE_NAME, QUEUE_OPTS);

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

    channel.consume(queue, msg => {
      fs.writeFile('res.pdf', msg.content, ioError => {
        if (ioError) throw ioError;

        pdfToImages('res.pdf');
      });
    }, { noAck: true });
  });
});

async function pdfToImages(filename) {
  const command = `pdftoppm ${filename} -jpeg -r 150 -f 1 -singlefile`
  const { stdout, stderr } = await exec(command, { maxBuffer: 1024 * 2000 });
  console.log('stdout:', stdout);
  console.log('stderr:', stderr);
}