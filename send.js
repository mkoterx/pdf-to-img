var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'hello';
        var msg = 'Hello World!';

        channel.assertQueue(queue, {
            durable: false
        });

        const fs = require('fs');

		fs.readFile('file.pdf', (err,data) => {
		    if (!err) {
	           channel.sendToQueue(queue, data);
		    } else {
		        console.log(err);
		    }
		});
    });
});